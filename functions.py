
def update_points(points, dt, springs):
    for spring in springs:
        spring.update(points, dt)

