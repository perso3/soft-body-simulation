from BasicVector import Vec2


class Spring:

    def __init__(self, point_a_index, point_b_index, stiffness, rest_length, damping_factor):
        self.point_a_index = point_a_index
        self.point_b_index = point_b_index

        self.stiffness = stiffness
        self.rest_length = rest_length
        self.damping_factor = damping_factor

    def update(self, points, dt):
        point_a = points[self.point_a_index[0]][self.point_a_index[1]]
        point_b = points[self.point_b_index[0]][self.point_b_index[1]]

        fs = self.stiffness * (Vec2.dist(point_b.pos, point_a.pos) - self.rest_length)

        normalized_direction_vector = (point_b.pos - point_a.pos) / Vec2.dist(point_b.pos, point_a.pos)
        velocity_difference = point_a.velocity - point_b.velocity

        dot_product = normalized_direction_vector.x * velocity_difference.x + normalized_direction_vector.y * velocity_difference.y

        fd = dot_product * self.damping_factor

        ft = fs + fd

        a = ((point_b.pos - point_a.pos) / Vec2.dist(point_b.pos, point_a.pos))
        a.mult(ft, ft)
        point_a.update(dt, a)

        b = ((point_a.pos - point_b.pos) / Vec2.dist(point_a.pos, point_b.pos))
        b.mult(ft, ft)
        point_b.update(dt, b)

    def __repr__(self):
        return f'{self.point_a_index, self.point_b_index}'
