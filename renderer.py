import pygame


def clear(window):
    window.fill((0, 0, 0))


def render(window, points, springs):
    clear(window)
    render_springs(window, points, springs)
    render_points(window, points)


def render_points(window, points):
    color = (255, 0, 0)
    for y, row in enumerate(points):
        for x, point in enumerate(row):
            pos = point.pos.getPos()

            pygame.draw.circle(window, color, pos, 2)


def render_springs(window, points, springs):
    color = (255, 255, 255)
    for spring in springs:
        a_index = spring.point_a_index
        b_index = spring.point_b_index

        start = points[a_index[0]][a_index[1]].pos.getPos()
        end = points[b_index[0]][b_index[1]].pos.getPos()

        pygame.draw.line(window, color, start, end)
