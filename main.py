import pprint
import random

import pygame
from pygame.locals import *
import time
from BasicVector import Vec2

from point import Point
from spring import Spring
from renderer import render

from functions import *


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    x_point_count = 10
    y_point_count = 5

    point_mass = 1

    spring_length = 50
    spring_stiffness = 5
    spring_rest_length = 55
    spring_dumping_factor = -2

    points = []

    x_offset = 50
    y_offset = 50

    for y in range(y_point_count):
        row = []
        for x in range(x_point_count):
            pin = False

            if y == 0:
                pin = True

            row.append(Point(Vec2(x_offset + spring_length * x, y_offset + spring_length * y), point_mass, pin))
        points.append(row)

    pprint.pprint(points)

    patterns = [(-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]

    springs = []

    for y, row in enumerate(points):
        for x, point in enumerate(row):
            for pattern in patterns:
                x_off = pattern[0]
                y_off = pattern[1]
                new_x = x + x_off
                new_y = y + y_off
                if new_x >= 0 and new_y >= 0:
                    if new_x < x_point_count and new_y < y_point_count:
                        springs.append(Spring([y, x], [new_y, new_x], spring_stiffness, spring_rest_length, spring_dumping_factor))

    pprint.pprint(springs)

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    choice = random.choice(springs)
                    springs.remove(choice)


        if not pause:
            pass
        if frameCount % 1 == 0:
            update_points(points, delta_time, springs)
            render(window, points, springs)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()