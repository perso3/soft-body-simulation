from BasicVector import Vec2

G = Vec2(0, 9.8)


class Point:

    def __init__(self, pos, mass, pin):
        self.pos = pos
        self.velocity = Vec2(0, 0)
        self.force = Vec2(0, 0)
        self.mass = mass
        self.pinned = pin

    def update(self, dt, spring_force):
        if not self.pinned:
            self.force = Vec2(0, 0)
            self.force += G * self.mass
            self.force += spring_force

            self.velocity += self.force * dt / self.mass

            if (self.pos + self.velocity * dt).y < 600:
                self.pos += self.velocity * dt

    def __repr__(self):
        return f' {self.pos.getPos()} {self.mass}'
